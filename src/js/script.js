import Parallax from 'parallax-js';
import Preloader from 'preloader';
const crel = require(`crel`);

const checkValidEmail = require(`./helpers/checkValidEmail`);

let prlxLongAndLatLeft, prlxLongAndLatRight, prlxTitle, prlxText, prlxForm, prlxMobile, prlxfallbackVideo, prlxBgGrids;
let longAndLatLeft, longAndLatRight, title, text, form, mobile, mobileId, fallbackVideo, fallbackVideoId, wrapper, btnSubmit, inputEmail, formClass, bgGrids;

let mouseX = window.innerWidth / 2,
  mouseY = window.innerHeight / 2;

let xPos = 0,
  yPos = 0;
let dX = 0,
  dY = 0;

const circle = document.getElementById(`circle`);

const ua = navigator.userAgent.toLowerCase();
//const isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);

window.mobilecheck = function() {
  let check = false;
  (function(a) {if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;})(navigator.userAgent || navigator.vendor || window.opera);
  return check;
};

const init = () => {

  longAndLatLeft = document.getElementById(`long-and-lat-left`);
  longAndLatRight = document.getElementById(`long-and-lat-right`);
  title = document.getElementById(`title`);
  text = document.getElementById(`text`);
  form = document.getElementById(`form`);
  formClass = document.querySelector(`.form`);
  bgGrids = document.getElementById(`background-grids`);
  mobile = document.querySelector(`.mobile-gyro`);
  fallbackVideo = document.querySelector(`.fallback-video`);
  fallbackVideoId = document.getElementById(`fallback-video`);
  // fallbackGif = document.querySelector(`.fallback-gif`);
  // fallbackGifId = document.getElementById(`fallback-gif`);
  wrapper = document.querySelector(`.wrapper`);
  btnSubmit = document.querySelector(`.btn-submit`);
  inputEmail = document.querySelector(`.input-email`);

  btnSubmit.addEventListener(`click`, handleSendForm, false);

  if (!window.getComputedStyle(document.body).mixBlendMode) {
    wrapper.style.display = `none`;
    fallbackVideo.style.display = `block`;
    fallbackVideo.style.boxShadow = `0 0 14rem 14rem black`;
    handleParallaxOnSafariAndIE();
  }

  const ua = navigator.userAgent.toLowerCase();
  if (ua.indexOf(`safari`) !== - 1) {
    if (ua.indexOf(`chrome`) > - 1) {
      //console.log(`Mix-blend-mode support`);
    } else {
      if (!window.mobilecheck()) {
        wrapper.style.display = `none`;
        fallbackVideo.style.display = `block`;
        fallbackVideo.style.boxShadow = `0 0 14rem 14rem black`;
        //handleParallaxOnSafari();
        handleParallaxOnSafariAndIE();
        console.log(fallbackVideo);

      } else {
        if (window.innerWidth < 775) {
          //console.log(`Let' go`);
        }
      }
      //console.log(`No mix-blend-mode support`);
    }
  }

  if (!window.mobilecheck()) {
    animate();
    handleParallax();
  } else {
    if (window.innerWidth < 775) {
      //console.log(`Let' go`);
      mobile.id = `mobile-gyro`;
      mobileId = document.getElementById(`mobile-gyro`);
      handleParallaxOnMobile();
    }
  }

};

const setMousePosition = e => {
  mouseX = e.pageX - 160;
  mouseY = e.pageY - 160;
};

const animate = () => {
  dX = mouseX - xPos;
  dY = mouseY - yPos;

  xPos += (dX / 40);
  yPos += (dY / 40);

  // circle.style.left = `${(window.innerWidth / 2) + (xPos / 2)}px`;
  // circle.style.top = `${100 + (yPos)}px`;

  circle.style.transform = `translate(${(window.innerWidth / 2) + (xPos / 2)}px, ${100 + (yPos)}px)`;

  requestAnimationFrame(animate);
};

const handleParallaxOnSafariAndIE = () => {
  prlxfallbackVideo = new Parallax(fallbackVideoId, {
    relativeInput: true,
    calibrate: false
  });

  prlxfallbackVideo.friction(0.04);
};
//
// const handleParallaxOnSafari = () => {
//   prlxfallbackGif = new Parallax(fallbackGifId, {
//     relativeInput: true,
//     calibrate: false
//   });
//
//   prlxfallbackGif.friction(0.04);
// };

const handleParallaxOnMobile = () => {
  prlxMobile = new Parallax(mobileId, {
    relativeInput: true,
    calibrate: false
  });

  prlxMobile.friction(0.7);
};

const handleParallax = () => {
  prlxLongAndLatLeft = new Parallax(longAndLatLeft, {
    relativeInput: true,
    calibrate: false
  });

  prlxLongAndLatRight = new Parallax(longAndLatRight, {
    relativeInput: true,
    calibrate: false
  });

  prlxTitle = new Parallax(title, {
    relativeInput: true,
    calibrate: false
  });

  prlxText = new Parallax(text, {
    relativeInput: true,
    calibrate: false
  });

  prlxForm = new Parallax(form, {
    relativeInput: true,
    calibrate: false
  });

  prlxBgGrids = new Parallax(bgGrids, {
    relativeInput: true,
    calibrate: false
  });

  prlxLongAndLatLeft.friction(0.04);
  prlxLongAndLatRight.friction(0.04);
  prlxTitle.friction(0.04);
  prlxText.friction(0.04);
  prlxForm.friction(0.04);
  prlxBgGrids.friction(0.04);
};

const handleSendForm = e => {
  e.preventDefault();

  const errorMessage = document.querySelector(`.error-message`);

  if (!inputEmail.value) {
    if (formClass.classList.contains(`error`)) {
      //error
    } else {
      formClass.classList.add(`error`);
      errorMessage.style.opacity = 1;
    }
  } else {
    if (!checkValidEmail(inputEmail.value)) {
      formClass.classList.add(`error`);
      errorMessage.style.opacity = 1;
    } else {
      formClass.classList.remove(`error`);
      errorMessage.style.opacity = 0;
      formClass.submit();
    }
  }
};

document.addEventListener(`mousemove`, setMousePosition, false);

const handleDOMMaskingGrid = () => {
  const element = crel(`div`, {id: `background-grids`, class: `full-size bg-divs`, 'data-relative-input': `true`},
    crel(`div`, {class: `animated-bg grid-green full-size`, 'data-depth': `-0.04`}),
    crel(`div`, {class: `animated-bg grid-blue full-size`, 'data-depth': `-0.04`}),
    crel(`div`, {class: `animated-bg grid-orange full-size`, 'data-depth': `-0.04`})
  );
  document.querySelector(`.bg-divs`).appendChild(element);
};

const handleDOMVideoGrid = () => {
  const element = crel(`div`, {class: `fallback-video`, 'data-depth': `-0.05`},
    crel(`video`, {src: `/assets/video/kronar_grid_video.mp4`, type: `video/mp4`, width: `700`, height: `700`, autoplay: ``, loop: ``, muted: ``, 'plays-inline': ``}
    )
  );
  document.querySelector(`.fallback-video-wrapper`).appendChild(element);
};

const loader = new Preloader({
  xhrImages: false
});
loader.on(`complete`, function() {
  const preloader = document.querySelector(`.preloader`);
  const spinner = document.querySelector(`.spinner`);

  spinner.style.opacity = 0;

  setTimeout(() => {
    preloader.style.opacity = 0;
  }, 1000);

  setTimeout(() => {
    preloader.style.display = `none`;
  }, 3000);

  init();
});

loader.add(`/assets/img/arrow.png`);
loader.add(`/assets/img/favicon.png`);

if (window.getComputedStyle(document.body).mixBlendMode) {
  if (ua.indexOf(`safari`) !== - 1) {
    if (ua.indexOf(`chrome`) > - 1 || ua.indexOf(`firefox`) > - 1) {
      loader.add(`/assets/img/kronarGrid_01.png`);
      loader.add(`/assets/img/kronarGrid_2.png`);
      loader.add(`/assets/img/kronarGrid_3.png`);
      handleDOMMaskingGrid();
    } else {
      if (!window.mobilecheck()) {
        console.log(`test2`);
        loader.add(`/assets/video/kronar_grid_video.mp4`);
        console.log(`This safari bitch`);
        handleDOMVideoGrid();
      } else {
        loader.add(`/assets/img/kronarGrid_01.png`);
        loader.add(`/assets/img/kronarGrid_2.png`);
        loader.add(`/assets/img/kronarGrid_3.png`);
        handleDOMMaskingGrid();
      }
    }
  }
} else {
  loader.add(`/assets/video/kronar_grid_video.mp4`);
  handleDOMVideoGrid();
}

loader.load();
